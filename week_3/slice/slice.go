package main

import ( 
	"fmt"
	"strings"
	"strconv"
	"sort"
)

func main() {
	var s string
	var i int

	// Create an empty slice of size 3
	slice := make([]int, 3)

	// Slice the slice
	slice = slice[:0]

	// For loop
	for {

		// Read an integer	
		fmt.Print("Enter an integer: ")
		_, err := fmt.Scanf("%s", &s)
		if err != nil {
			fmt.Println(err)
			continue
		}

		// Verify it is not "X"
		if (strings.Compare(s, "X") == 0) {
			break;
		}

		// Convert string in integer
		i, err = strconv.Atoi(s)
		if err != nil {
			fmt.Println(err)
			continue
		}

		// Add the integer into the slice
		slice = append(slice, i)

		// Sort the slice
		sort.Ints(slice)

		// Print the content of the slice in sorted order
		fmt.Printf("[%d]int{", len(slice))
		fmt.Print(slice)
		fmt.Println("}")
	}
}
