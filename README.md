These are the programming assignments relative to Go - Getting Started

- Assignment 1: Hello, World! 
- Assignment 2-1: floating point numbers and truncation code
- Assignment 2-2: strings
- Assignment 3: integers, slices and loops
- Assignment 4-1: practice working with RFCs in Go, using JSON as an example
- Assignment 4-2: practice working with the ioutil and os packages

For more information, I invite you to have a look at https://www.coursera.org/learn/golang-getting-started
