package main

import ( 
	"bufio"
	"os"
	"fmt"
	"strings"
)

func main() {
	var s string

	// Read input sting
	consoleReader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter a string: ")
	s, err := consoleReader.ReadString('\n')
	if err != nil {
		fmt.Println("Not Found!")
		return
	}

	// Convert CRLF to LF
	s = strings.Replace(s, "\n", "", -1)
	
	// Convert in upper case
	s = strings.ToUpper(s)

	// Test if it stars with "I"
	if (!strings.HasPrefix(s, "I")) {
		fmt.Println("Not Found!")
		return
	}	
	
	// Test if it ends with "N"
	if (!strings.HasSuffix(s, "N")) {
		fmt.Println("Not Found!")
		return
	}
	
	// Test if it contains "A"
	if (!strings.Contains(s, "A")) {
		fmt.Println("Not Found!")
		return
	}

	// Found it
	fmt.Println("Found!")
}
