package main

import "fmt"

func main() {
	var f float32

	fmt.Print("Enter a float value : ")
	_, err := fmt.Scanf("%f", &f)
	if err != nil {
		fmt.Println(err)
	} else {	
		fmt.Printf("You have entered : %d\n", int(f))
	}
}
