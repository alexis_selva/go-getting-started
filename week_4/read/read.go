package main

import ( 
	"fmt"
	"os"
	"bufio"
)

type Person struct {
	Firstname string
	Lastname string
}

func main() {
	var path string

	// Read the name	
	fmt.Print("Enter the name of the text file: ")
	_, err := fmt.Scanf("%s", &path)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Open the file
	file, err := os.Open(path)
	if err != nil {
		return
	}

	// Create the slice
    slice := make([]Person, 0)

	// Walk through the words
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords) 
	for scanner.Scan() {
		
		// Read the firstname
		firstname := scanner.Text()

		// Read the lastname
		scanner.Scan()
		lastname := scanner.Text()

		// Create a struct
		slice = append(slice, Person{Firstname: firstname, Lastname: lastname})
	}

	// Close the file
	file.Close()

	for _, p := range slice {
		fmt.Println(p.Firstname + ", " + p.Lastname)
	}
}
