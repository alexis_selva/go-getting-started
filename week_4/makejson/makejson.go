package main

import ( 
	"fmt"
	"encoding/json"
)

type Person struct {
	Name string
	Address string
}

func main() {
	var n string
	var a string

	// Read the name	
	fmt.Print("Enter a name: ")
	_, err := fmt.Scanf("%s", &n)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Read the address
	fmt.Print("Enter an address: ")
	_, err = fmt.Scanf("%s", &a)
	if err != nil {
		fmt.Println(err)
		return
	}
	
	// Create a struct
	user := Person{Name: n, Address: a}

	// Create the map
    m := make(map[string]Person)

	// Add the name and the address to the map
	m[n+a] = user

	// Marshal the map
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))
}
